﻿
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;

namespace SamlTest.Controllers
{
     [Authorize]
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            var claims = ClaimsPrincipal.Current.Claims.ToList();
            var claimsPrincipal = HttpContext.User as ClaimsPrincipal;
            var isAuthenticated = claimsPrincipal.Identity.IsAuthenticated;
            var token = GenerateLocalAccessTokenResponse(claims.ToList());
            ViewData["user_access_token"] = token;

            return View();
        }

        private string GenerateLocalAccessTokenResponse(List<Claim> claims)
        {
            var tokenExpiration = TimeSpan.FromDays(1);

            ClaimsIdentity identity = new ClaimsIdentity(OAuthDefaults.AuthenticationType);

            identity.AddClaim(new Claim(ClaimTypes.Name, "Inderjeet"));

            foreach (var item in claims)
            {
                identity.AddClaim(new Claim(item.Type, item.Value));
            }

            var props = new AuthenticationProperties()
            {
                IssuedUtc = DateTime.UtcNow,
                ExpiresUtc = DateTime.UtcNow.Add(tokenExpiration),
            };

            var ticket = new AuthenticationTicket(identity, props);

            var accessToken = Startup.OAuthOptions.AccessTokenFormat.Protect(ticket);

            return accessToken;
        }
    }
}